<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('ModelStudent');
		$biodata['query'] = $this->ModelStudent->GetBiodata();
		// print_r($biodata);exit();
		$this->load->view('admin/include/header');
		$this->load->view('admin/student', $biodata);
		$this->load->view('admin/include/footer');
	}
	public function StudentCreate(){
		// print_r($this->input->post());exit();
		$save_data = array(
					'name' => $this->input->post('name', TRUE),
					'ttl' => $this->input->post('ttl', TRUE),
					'telp' => $this->input->post('telp', TRUE),
					'alamat' => $this->input->post('alamat', TRUE),
					'gender' => $this->input->post('gender', TRUE),
		);
			print_r($save_data);exit();
			$this->load->model('ModelStudent');
			$this->ModelStudent->SaveBiodata($save_data);
	}


}
