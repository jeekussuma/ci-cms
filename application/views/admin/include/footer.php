 <!-- /.panel-body -->
                        <div class="panel-footer">
                            <div class="input-group">

                            </div>
                        </div>
                        <!-- /.panel-footer -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/admin/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/data/morris-data.js"></script>
    <script src="<?php echo base_url()?>assets/admin/dist/js/sb-admin-2.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</body>
</html>

<script>
$('document').ready(function(){
    $('#table-student').DataTable();
    $('#show-form').hide();
});
$('#add-modal-student').on('click', function(){
    $('#show-table').hide();
    $('#show-form').show(100);
});
$('#simpan').on('click', function(){
      $('#show-form').hide();
    $('#show-table').show(100);
  
});
$('#batal').on('click', function(){
    $('#show-table').show();
    $('#show-form').hide();
});

$('#tanggal_lahir').on('change', function(){
    var tempatLahir = $('#tempat_lahir').val();
    var tanggalLahir = $('#tanggal_lahir').val();
    $('#ttl').val(tempatLahir+' '+tanggalLahir);
    // alert(tempatLahir+' '+tanggalLahir);
    // if(tanggalLahir !=''){alert($('#ttl').val('tempatLahir'+' '+'tanggalLahir'))}
});


</script>
