<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><button id="add-modal-student" type="button" class="btn btn-primary">Tambah Data</button></h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" id="show-table">
            <div class="panel-heading">
                Data Siswa
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="table-student">
                    <thead>
                        <tr>
                            <th>Id Santri</th>
                            <th>Nama Lengkap</th>
                            <th>Telp</th>
                            <th>ttl</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($query as $student) { ?>
                        <tr class="odd gradeX">
                            <td><?php echo $student->id_santri; ?></td>
                            <td><?php echo $student->nama; ?></td>
                            <td><?php echo $student->telp; ?></td>
                            <td><?php echo $student->ttl; ?></td>
                            <td class="center">X</td>
                        </tr>
                        <?php } ?>
               
                    </tbody>
                </table>
                
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        <div class="panel panel-default" id="show-form">
            <div class="panel-heading">
               Tambah Data
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <div class="row">
                                <div class="col-lg-4">
                                    <form role="form" method="post" action="<?php echo site_url('student/StudentCreate')?>">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input class="form-control" name="nama">
                                        </div>
                                        <div class="form-group">
                                            <label>Tempat Tanggal Lahir</label>
                                            <input type="text" class="form-control" id="tempat_lahir" placeholder="Tempat Lahir">
                                            <input type="text" class="form-control" id="tanggal_lahir" placeholder="Tanggal" style="margin-top:5px">
                                            <input type="hidden" class="form-control" name="ttl">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Telp</label>
                                            <input class="form-control" name="telp" placeholder="Telp/WA">
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select class="form-control" name="gender">
                                                <option>Laki-laki</option>
                                                <option>Perempuan</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Alamat Lengkap</label>
                                            <textarea class="form-control" rows="3" name="alamat-lengkap"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Foto</label>
                                            <input type="file" name="tipe-file">
                                        </div>
                                        <button id="simpan" type="submit" class="btn btn-default">Simpan </button>
                                        <button id="batal" type="button" class="btn btn-default">Batal</button>
                                    </form>

                
                <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

</div>
<!-- /#page-wrapper -->